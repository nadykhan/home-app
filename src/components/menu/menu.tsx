import React, { useEffect } from 'react';
import MenuTable from '../../layouts/menu-table/menu-table'
import classes from './menu.module.scss'

let menuItems = [
    { path: '/', activeClass: classes.ActiveLink, value: 'Home', exact: true },
    { path: '/skills', activeClass: classes.ActiveLink, value: 'Skills', exact: false },
    { path: '/work', activeClass: classes.ActiveLink, value: 'Work', exact: false },
    { path: '/experience', activeClass: classes.ActiveLink, value: 'Experience', exact: false },
    { path: '/education', activeClass: classes.ActiveLink, value: 'Education', exact: false },
]


const Menu = (props: any) => {

    useEffect(() => {

    }, [''])

    return (
        <MenuTable data={menuItems} />
    )
}


export default Menu;