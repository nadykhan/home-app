import React from 'react';
import classes from './app.module.scss';
import Menu from '../menu/menu';
import Skills from '../skills/skills';
import Home from '../home/home'
import { BrowserRouter, Route } from 'react-router-dom';
import Profile from '../../../src/n.jpeg'
import Experience from '../experience/experience'


const App = (props: object) => {
    return (
        <BrowserRouter>
            <div className={classes.Menu}>
                <div className={classes.Sidetable}>
                    <div className={classes.Profile}>
                        <div className={classes.ProfileImg}>
                            <img src={Profile} alt='Profile' />
                        </div>
                        <div className={classes.Name}>Nadeem Ahmad</div>
                        <span className={classes.Prof}>Full Stack Engineer</span>
                    </div>
                    <Menu />
                </div>
                <div className={classes.RightContent}>
                    <Route path='/' exact component={Home} />
                    <Route path='/skills' component={Skills} />
                    <Route path='/experience' component={Experience} />
                </div>
            </div>
        </BrowserRouter>

    )
}


export default App;