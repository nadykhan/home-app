import React from 'react'
import Section from '../../../layouts/section/section'
import classes from './about-me.module.scss'
import SocialProfiles from '../social-profiles/social-profiles'

const AboutMe = (props: any) => {
    return (
        <div>
            <Section head={'About Me'}>
                <div className={classes.ProfileSummary}>
                    Experienced Software Engineer with 8 years of experience designing,
                    developing, and deploying robust code for high-volume businesses in
                    a highly collaborative, agile, and fast-paced environment. Possesses
                    excellent communication skills, bringing forth extensive experience
                    in performing research on product development processes and offering
                    solutions and alterations to improve performance and scalability.
                    I always have the pleasure to acquire valuable knowledge, diversified
                    skills, strong leadership, and managerial capabilities to complement.
                </div>
            </Section>
            <SocialProfiles />
        </div>
    )
}


export default AboutMe