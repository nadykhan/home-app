import React from 'react';
import classes from './social-profiles.module.scss'
import Section from '../../../layouts/section/section'
import SocialCard from '../../../layouts/social-card/social-card'

const socialLinks = [
    {
        icon: 'https://cdn3.iconfinder.com/data/icons/inficons/512/stackoverflow.png',
        link: '',
        name: 'Stackoverflow'
    },
    {
        icon: 'https://image.flaticon.com/icons/png/512/124/124021.png',
        link: '',
        name: 'Twitter'
    },
    {
        icon: 'https://cdn.visualbi.com/wp-content/uploads/linkedin-icon.png',
        link: '',
        name: 'LinkedIn'
    },
    {
        icon: 'https://cdn4.iconfinder.com/data/icons/miu-square-flat-social/60/youtube-square-social-media-512.png',
        link: '',
        name: 'YouTube'
    },
    {
        icon: 'https://image.flaticon.com/icons/svg/38/38401.svg',
        link: '',
        name: 'GitHub'
    },
    {
        icon: 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/GitLab_Logo.svg/1200px-GitLab_Logo.svg.png',
        link: '',
        name: 'GitLab'
    },
    {
        icon: 'https://info.hackerrank.com/rs/487-WAY-049/images/Podcast-ChannelCover-Final.jpg',
        link: '',
        name: 'Hackerrank'
    },
    {
        icon: 'https://cdn4.iconfinder.com/data/icons/social-media-2210/24/Medium-512.png',
        link: '',
        name: 'Medium'
    },
    {
        icon: 'https://icons.iconarchive.com/icons/papirus-team/papirus-apps/512/upwork-icon.png',
        link: '',
        name: 'Upwork'
    },
    {
        icon: 'https://cdn4.iconfinder.com/data/icons/liu-square-blac/60/behance-square-social-media-512.png',
        link: '',
        name: 'Behance'
    },
]

const SocialProfiles = (props: any) => {
    return (
        <Section head={'Social Profiles'}>
            <div className={classes.SocialProfilesGrid}>
                {
                    socialLinks.map(link => {
                        return <SocialCard data={link} />
                    })
                }
            </div>
        </Section>
    )
}

export default SocialProfiles