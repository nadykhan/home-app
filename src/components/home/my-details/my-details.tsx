import React from 'react';
import classes from './my-details.module.scss';
import Section from '../../../layouts/section/section'
import { calcDiff } from '../../../services/Date/calculate-difference'

const MyDetails = (props: any) => {
    return (
        <div>
            <Section head={'Contact Information'}>
                <div className={classes.DetailsWrap}>
                    <ul>
                        <li>
                            <span>Name</span><span>Nadeem Ahmad</span>
                        </li>
                        <li>
                            <span>Age</span><span>{calcDiff('06 29 1995')}</span>
                        </li>
                        <li>
                            <span>Resident</span><span>Pakistan</span>
                        </li>
                        <li>
                            <span>Address</span><span>Swat, KP, Pakistan</span>
                        </li>
                        <li>
                            <span>Phone</span><span>+92 (347) 192 0264</span>
                        </li>
                        <li>
                            <span>Email</span><span>nadeem.ahmad.na@outlook.com</span>
                        </li>
                        <li>
                            <span>Languages Known</span><span>English, Urdu, Pashto</span>
                        </li>
                        <li>
                            <span>Work Status</span><span>Available</span>
                        </li>
                        <li>
                            <span>Work Preferences</span><span>Remote or Onsite (Sponsorship Requried)</span>
                        </li>
                        <li>
                            <span>Availability</span><span>Full Time (40 hrs / week)</span>
                        </li>
                        <li>
                            <span>Last Employer</span><span>IBM</span>
                        </li>
                        <li>
                            <span>Work Experience</span><span>8 Years</span>
                        </li>
                    </ul>
                </div>
            </Section>
        </div>
    )
}

export default MyDetails;