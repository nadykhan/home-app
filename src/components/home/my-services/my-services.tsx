import React from 'react';
import classes from './my-services.module.scss';
import Section from '../../../layouts/section/section';
import { NavLink } from 'react-router-dom';

const services = [
    {
        icon: 'https://icons-for-free.com/iconfiles/png/512/ui+ux+web+web+design+icon-1320166165572916697.png',
        name: 'UI / UX Designing',
        desc: 'Gathering and evaluating user requirements, in collaboration with product managers and engineers. Illustrating design ideas using storyboards, process flows and sitemaps. Designing graphic user interface elements, like menus, tabs and widgets.'
    },
    {
        icon: 'https://image.flaticon.com/icons/png/512/186/186303.png',
        name: 'Development',
        desc: `I have plenty of experience working as a Full Stack Developer. Though, I have worked as a full stack I am mostly front end oriented and more proficient in the front end technologies.`
    },
    {
        icon: 'https://www.ibm.com/blogs/cloud-archive/wp-content/uploads/2017/06/continuous-delivery-hero.png',
        name: 'Application Deployment',
        desc: 'Good hands-on knowledge of Source Code Management (Version Control System) tools like Git and Subversion. Proficient in developing Continuous Integration/ Delivery pipelines. Experience with automation/ integration tools like Jenkins.'
    },
]

const MyServices = (props: any) => {
    return (
        <Section head={'What I can do ?'}>
            <div className={classes.ServicesGrid}>
                {
                    services.map((service, index) => {
                        return (
                            <div className={classes.ServiceWrap} key={index}>
                                <div className={classes.ServicesIcon}>
                                    <img src={service.icon} alt={service.name} />
                                </div>
                                <div className={classes.ServiceName}>{service.name}</div>
                                <div className={classes.ServiceDesc}>{service.desc}</div>
                            </div>
                        )
                    })
                }
            </div>
        </Section>
    )
}



export default MyServices;