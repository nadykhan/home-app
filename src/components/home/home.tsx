import React from 'react';
import AboutMe from './about-me/about-me';
import MyDetails from './my-details/my-details';
import MyServices from './my-services/my-services';
import classes from './home.module.scss';

const Home = (props: object) => {
    return (
        <>
            <div className={classes.HomeGrid}>
                <AboutMe />
                <MyDetails />
            </div>
            <MyServices />
        </>
    )
}



export default Home;