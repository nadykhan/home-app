import React, { useEffect } from 'react';
import classes from './skills.module.scss';
import Card from '../../layouts/skill-card/skill-card';
import { calcDiff } from '../../services/Date/calculate-difference'


const skills = [
    {
        head: "Web Designing",
        data: [
            { name: 'HTML 4/5', img: 'https://cdn1.iconfinder.com/data/icons/logotypes/32/badge-html-5-512.png', experienc: '03 29 2012' },
            { name: 'CSS3', img: 'https://www.kindpng.com/picc/m/464-4640184_css3-png-download-css-icon-transparent-png.png', experienc: '03 29 2012' },
            { name: 'SASS/SCSS', img: 'https://cdn3.iconfinder.com/data/icons/logos-and-brands-adobe/512/288_Sass-512.png', experienc: '04 12 2019' },
            { name: 'Bootstrap', img: 'https://pluspng.com/img-png/bootstrap-logo-png-open-2000.png', experienc: '10 15 2012' },
            { name: 'Semantic UI', img: 'https://react.semantic-ui.com/logo.png', experienc: '08 04 2019' },
            { name: 'Ant Design', img: 'https://gw.alipayobjects.com/zos/rmsportal/rlpTLlbMzTNYuZGGCVYM.png', experienc: '12 20 2019' },
            { name: 'Materialize CSS', img: 'https://colinstodd.com/images/posts/matcss-min.png', experienc: '04 03 2018' },
            { name: 'Figma', img: 'https://miro.medium.com/max/670/0*UTBrDcrJ6SbePBzR', experienc: '01 03 2019' },
            { name: 'Sketch', img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/59/Sketch_Logo.svg/1133px-Sketch_Logo.svg.png', experienc: '11 01, 2019' },
            { name: 'Zeplin', img: 'https://lever-client-logos.s3.amazonaws.com/bbeb928f-41e7-4b35-9d43-738bb826bffa-1549834424700.png', experienc: '11 01, 2019' },
            { name: 'Invision', img: 'https://cdn.worldvectorlogo.com/logos/invision.svg', experienc: '06 09 2016' },
            { name: 'Styled Components', img: 'https://www.styled-components.com/atom.png', experienc: '02 21 2019' },
            { name: 'CSS Modules', img: 'https://www.insidersbyte.com/static/css-modules-logo-318db3e96c54ca8fe5a40d2cc1b5d821-b64db.png', experienc: '12 12 2018' },
            { name: 'JSS', img: 'https://raw.githubusercontent.com/jsstyles/logo/master/logo.png', experienc: '03 14 2020' },
            { name: 'Radium', img: 'https://d2eip9sf3oo6c2.cloudfront.net/tags/images/000/001/092/full/radiumlogo.png', experienc: '05 17 2020' },
        ]
    },
    {
        head: "Front End",
        data: [
            { name: 'JavaScript', img: 'https://cdn.worldvectorlogo.com/logos/javascript.svg', experienc: '07 15 2012' },
            { name: 'ES6+', img: 'https://raw.githubusercontent.com/wingsuitist/ecmascript-logo/master/es-ecmascript-logo.png', experienc: '07 10 2016' },
            { name: 'Jquery', img: 'https://pluspng.com/img-png/jquery-logo-png--800.gif', experienc: '09 21 2014' },
            { name: 'TypeScript', img: 'https://cdn.worldvectorlogo.com/logos/typescript.svg', experienc: '09 27 2017' },
            { name: 'React JS', img: 'https://cdn4.iconfinder.com/data/icons/logos-3/600/React.js_logo-512.png', experienc: '04 06 2016' },
            { name: 'Redux', img: 'https://www.vergic.com/wpsitefiles_de3fxs/wp-content/uploads/2017/04/logo.png', experienc: '07 16 2016' },
            { name: 'GraphQL', img: 'https://raw.githubusercontent.com/rohan-varma/rohan-blog/gh-pages/images/graphql.png', experienc: '06 12 2019' },
            { name: 'Angular 2+', img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Angular_full_color_logo.svg/250px-Angular_full_color_logo.svg.png', experienc: '05 20 2017' },
            { name: 'NGRX', img: 'https://ngrx.io/assets/images/badge.svg', experienc: '08 08 2018' },
            { name: 'RxJS', img: 'https://ih1.redbubble.net/image.392874831.7635/st,small,845x845-pad,1000x1000,f8f8f8.jpg', experienc: '07 11 2019' },
            { name: 'VueJS', img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/Vue.js_Logo_2.svg/1200px-Vue.js_Logo_2.svg.png', experienc: '06 21 2018' },
            { name: 'VueX', img: 'https://user-images.githubusercontent.com/7110136/29002857-9e802f08-7ab4-11e7-9c31-604b5d0d0c19.png', experienc: '06 30, 2018' },
            { name: 'NuxtJS', img: 'https://nuxtjs.org/logos/nuxt-icon-white.png', experienc: '09 28 2019' },
            { name: 'Ember JS', img: 'https://emberjs.com/images/brand/ember_Tomster-Lockup.png', experienc: '02 01 2020' },
            { name: 'Backbone JS', img: 'https://techchurian.files.wordpress.com/2013/02/backbone_logo_logo_only.png', experienc: '04 19 2020' },
            { name: 'Meteor JS', img: 'https://www.arkasoftwares.com/images/android/meteorjsLogo.png', experienc: '05 04 2020' },
            { name: 'Polymer', img: 'https://cdn.worldvectorlogo.com/logos/polymer.svg', experienc: '06 20 2020' },
            { name: 'Three.JS', img: 'https://ingenuitysoftwarelabs.com/wp-content/uploads/2020/01/three-js-logo.png', experienc: '10 22 2018' },
            { name: 'Chart JS', img: 'https://www.chartjs.org/img/chartjs-logo.svg', experienc: '11 29 2017' },
            { name: 'Babylon JS', img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/8e/Babylon_logo_v4.svg/1200px-Babylon_logo_v4.svg.png', experienc: '11 11 2019' },
            { name: 'JSX', img: 'https://raw.githubusercontent.com/jsx-ir/logo/master/jsx.png', experienc: '04 06 2016' },
            { name: 'Babel', img: 'https://pbs.twimg.com/profile_images/567000326444556290/-1wfGjNw_400x400.png', experienc: '11 01 2017' },
        ]
    },
    {
        head: "Back End",
        data: [
            { name: 'Node JS', img: 'https://logodix.com/logo/1764882.png', experienc: '01 12 2017' },
            { name: 'Express JS', img: 'https://virajkadam.com/wp-content/uploads/2019/09/expressjs.png', experienc: '01 10 2018' },
            { name: 'Next JS', img: 'https://upload-icon.s3.us-east-2.amazonaws.com/uploads/icons/png/9114856761551941711-512.png', experienc: 'Jan 21 2019' },
            { name: 'Python', img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Python-logo-notext.svg/1024px-Python-logo-notext.svg.png', experienc: '09 18 2016' },
            { name: 'DJango', img: 'https://cdn.iconscout.com/icon/free/png-512/django-2-282855.png', experienc: '04 09 2018' },
            { name: 'Flask', img: 'https://cdn.hashnode.com/res/hashnode/image/upload/v1518503935975/S1_-_WePM.png', experienc: '11 29 2018' },
            { name: 'PHP', img: 'https://git.shitware.xyz/uploads/-/system/project/avatar/484/icon-tag-php-8a7ff308169b2111c5a6e8e7509c10d50f9704252f82ab24aee756cf9edf5351.png', experienc: '06 29 2014' },
            { name: 'Laravel', img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Laravel.svg/1200px-Laravel.svg.png', experienc: '12 18 2016' },
            { name: 'Code Igniter', img: 'https://cdn1.iconfinder.com/data/icons/logos-3/304/codeigniter-icon-512.png', experienc: '10 21, 2017' },
            { name: 'Socket.io', img: 'https://upload.wikimedia.org/wikipedia/commons/9/96/Socket-io.svg', experienc: '05 11, 2017' },
        ]
    },
    {
        head: "Databases & Caching",
        data: [
            { name: 'MySql', img: 'https://pngimg.com/uploads/mysql/mysql_PNG23.png', experienc: '07 01 2014' },
            { name: 'PostgreSQL', img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/Postgresql_elephant.svg/1200px-Postgresql_elephant.svg.png', experienc: '02 17 2018' },
            { name: 'Maria DB', img: 'https://www.softizy.com/blog/wp-content/uploads/2014/05/mariadb.png', experienc: '08 18 2019' },
            { name: 'MongoDB', img: 'https://infinapps.com/wp-content/uploads/2018/10/mongodb-logo.png', experienc: '10 14 2016' },
            { name: 'CouchBase', img: 'https://api.nuget.org/v3-flatcontainer/couchbaseidentity/1.0.1-dp/icon', experienc: '11 21 2019' },
            { name: 'DynamoDB', img: 'https://cdn.worldvectorlogo.com/logos/aws-dynamodb.svg', experienc: '02 11 2020' },
            { name: 'Redis', img: 'https://cdn.worldvectorlogo.com/logos/redis.svg', experienc: '06 01 2017' },
            { name: 'Memcached', img: 'https://dwglogo.com/wp-content/uploads/2017/12/Memcached_logo.png', experienc: '01 21 2020' },
            { name: 'Firebase', img: 'https://4.bp.blogspot.com/-rtNRVM3aIvI/XJX_U07Z-II/AAAAAAAAJXY/YpdOo490FTgdKOxM4qDG-2-EzcNFAWkKACK4BGAYYCw/s1600/logo%2Bfirebase%2Bicon.png', experienc: '09 23 2018' },
            { name: 'Sequelize', img: 'https://miro.medium.com/max/250/1*-6qHuOwJfWtkFkymMovJFQ.png', experienc: '04 10 2018' },
            { name: 'SQL Alchemy', img: 'https://www.saashub.com/images/app/service_logos/22/7f2218e17a20/large.png?1545367213', experienc: '07 01 2018' },
            { name: 'Doctrine', img: 'https://cdn.freebiesupply.com/logos/thumbs/2x/doctrine-logo.png', experienc: '02 03 2017' },
        ]
    },
    {
        head: "Task Automation, Unit Testing tools & Package Managers",
        data: [
            { name: 'Jest', img: 'https://seeklogo.com/images/J/jest-logo-F9901EBBF7-seeklogo.com.png', experienc: '03 22 2018' },
            { name: 'Mocha', img: 'https://banner2.cleanpng.com/20180427/ivw/kisspng-mocha-node-js-javascript-software-testing-npm-5ae37d55ae0302.6235689115248581977128.jpg', experienc: '08 02 2018' },
            { name: 'Enzyme', img: 'https://s.gravatar.com/avatar/a78ef807c3154e902c98d6c3834f0577?size=496&default=retro', experienc: '06 02 2019' },
            { name: 'Karma', img: 'https://bkimminich.github.io/juice-shop/assets/karma.png', experienc: '06 12 2018' },
            { name: 'WebPack', img: 'https://raw.githubusercontent.com/webpack/media/master/logo/icon-square-big.png', experienc: '01 21 2018' },
            { name: 'Gulp', img: 'https://miro.medium.com/proxy/1*IGn5E-1wp5mQ2DHoevVCFA.png', experienc: '09 02 2018' },
            { name: 'Grunt', img: 'https://www.logolynx.com/images/logolynx/66/6656cdbc4dff3af41e60ffeaede8f095.jpeg', experienc: '06 14 2019' },
            { name: 'NPM', img: 'https://hashtagcauseascene.com/wp-content/uploads/2018/07/npm-logo-red_tn.jpg', experienc: '09 19 2016' },
            { name: 'YARN', img: 'https://pbs.twimg.com/profile_images/778422085639032832/44mC-kJ3.jpg', experienc: '01 08 2017' },
            { name: 'Composer', img: 'https://idroot.us/wp-content/uploads/2019/12/Composer-logo.png', experienc: '01 15 2017' },
            { name: 'PIP', img: 'https://partners.sigfox.com/assets/logo-for/59ba6d5e220dca00011743fe', experienc: '10 11 2017' },
        ]
    },
    {
        head: "Microservices Architecture Techs & Tools",
        data: [
            { name: 'Microservices', img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcS_Xy2C2FhM3EEThoxTlaXgNqOGGvLL_iErcA&usqp=CAU', experienc: '02 05 2019' },
            { name: 'RabbitMQ', img: 'https://aphyr.com/data/posts/315/RabbitMQ.sh-600x600.png', experienc: '04 15 2019' },
            { name: 'Apache Kafka', img: 'https://d15shllkswkct0.cloudfront.net/wp-content/blogs.dir/1/files/2017/11/kazUJooF_400x400.jpg', experienc: '10 01 2019' },
            { name: 'Docker', img: 'https://cdn4.iconfinder.com/data/icons/logos-and-brands/512/97_Docker_logo_logos-512.png', experienc: '03 25 2019' },
            { name: 'Kubernetes', img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/3/39/Kubernetes_logo_without_workmark.svg/1200px-Kubernetes_logo_without_workmark.svg.png', experienc: '08 06 2019' },
            { name: 'Jenkins', img: 'https://dyltqmyl993wv.cloudfront.net/assets/stacks/jenkins/img/jenkins-stack-220x234.png', experienc: '02 02 2019' },
            { name: 'DevOps', img: 'https://cdn1.iconfinder.com/data/icons/devops-cycle/256/devops_cycle_1_flat-512.png', experienc: '01 01 2019' },
            { name: 'CI/CD', img: 'https://about.gitlab.com/images/ci/gitlab-ci-cd-logo_2x.png', experienc: '03 11 2018' },
            { name: 'Ansible', img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/24/Ansible_logo.svg/1200px-Ansible_logo.svg.png', experienc: '06 11 2019' },
            { name: 'Elastic Search', img: 'https://punchplatform.com/wp-content/uploads/2017/12/elastic-logo-200.png', experienc: '11 21 2019' },
            { name: 'Kibana', img: 'https://3.bp.blogspot.com/-JCZ7s-OSCHE/XJaylDDOUQI/AAAAAAAAJZs/oW0dDzXnUqQXpfjvQjbsOG4q5T9gWzSEACK4BGAYYCw/s1600/logo%2Belastic%2Bkibana%2Bicon.png', experienc: '01 09 2020' },
            { name: 'Logstash', img: 'https://cdn.worldvectorlogo.com/logos/elastic-logstash.svg', experienc: '01 03 2020' },
        ]
    },
    {
        head: "Software Development Methodologies",
        data: [
            { name: 'Agile Dev.', img: 'https://static1.squarespace.com/static/56516ae2e4b058e88fc6cbfc/5667fec3a12f443affb3538e/5a72cb69652deac3c38a05c8/1520924065025/AAIA_wDGAAAAAQAAAAAAAApKAAAAJDhkZTdjYzBlLWUzNWItNDU5ZS05ZTA1LWMzMzZiNTAxNDVkOQ.jpg?format=1500w', experienc: '05 08 2016' },
            { name: 'SCRUM', img: 'https://www.scrum.org/themes/custom/scrumorg/assets/images/logo-250.png', experienc: '03 09 2017' },
        ]
    },
    {
        head: "Web Scrapping, Authentication & APIS",
        data: [
            { name: 'Ajax', img: 'https://www.w3schools.com/whatis/img_ajax.jpg', experienc: '05 25 2015' },
            { name: 'JSON', img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/c9/JSON_vector_logo.svg/1200px-JSON_vector_logo.svg.png', experienc: '01 05 2015' },
            { name: 'REST', img: 'https://rest.com.au/Content/assets/images/Facebook-Image.png', experienc: '10 02 2014' },
            { name: 'SOAP', img: 'https://static1.squarespace.com/static/51814c87e4b0c1fda9c1fc50/51814c87e4b0c1fda9c1fc5a/58c8556dd482e9cafdc6e01a/1499997065644/soap-cloud-gear-thumbnail.jpg?format=1500w', experienc: '09 15 2017' },
            { name: 'OAuth', img: 'https://wiki.oauth.net/f/1199123688/oauth_logo_discovery.png', experienc: '06 21 2018' },
            { name: 'JWT', img: 'https://i2.wp.com/blog.logrocket.com/wp-content/uploads/2019/07/Screen-Shot-2018-10-11-at-1.40.06-PM.png?fit=1016%2C1034&ssl=1', experienc: '02 11 2017' },
            { name: 'OpenID', img: 'https://nat.sakimura.org/wp-content/uploads/2012/02/openid-icon-250x250.png', experienc: '02 22 2019' },
            { name: 'Casper JS', img: 'https://casperjs-dev.readthedocs.io/en/latest/_images/casperjs-logo.png', experienc: '09 17 2018' },
            { name: 'Puppeteer', img: 'https://img.stackshare.io/service/7553/puppeteer.png', experienc: '10 04 2019' },
            { name: 'Selenium', img: 'https://logodix.com/logo/1122060.jpg', experienc: '11 11 2018' },
        ]
    },
    {
        head: "Application Deployment",
        data: [
            { name: 'Microsoft Azure', img: 'https://i.pinimg.com/originals/64/1a/60/641a60c0f6d3522807916f078547c94f.png', experienc: '02 04 2019' },
            { name: 'AWS', img: 'https://pronto-core-cdn.prontomarketing.com/2/wp-content/uploads/sites/1614/2019/07/21743298_1406722539365107_4308832733562613967_n.png', experienc: '05 15 2018' },
            { name: 'Google Cloud', img: 'https://www.gstatic.com/devrel-devsite/prod/vc0d10ef7c6e8aac6c71e2a2051f66f30f3c99a4b52237746839ce4f1fae2b7b4/cloud/images/favicons/onecloud/apple-icon.png', experienc: '03 18 2019' },
            { name: 'Heroku', img: 'https://res.cloudinary.com/practicaldev/image/fetch/s--K2q0A5SX--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://thepracticaldev.s3.amazonaws.com/i/2elgd5zp07wkeilkna63.png', experienc: '09 19 2019' },
            { name: 'Digital Ocean', img: 'https://cdn.worldvectorlogo.com/logos/digitalocean-icon-1.svg', experienc: '06 06 2019' },
            { name: 'Bluehost', img: 'https://s.w.org/hosting/bluehost.png?3', experienc: '04 12 2018' },
        ]
    },
    {
        head: "Artificial Intelligence",
        data: [
            { name: 'Machine Learning', img: 'https://upload.wikimedia.org/wikipedia/commons/d/d5/Hey_Machine_Learning_Logo.png', experienc: '06 27 2019' },
            { name: 'Scikit-learn', img: 'https://amueller.github.io/ml-training-intro/slides/images/sklearn_logo.png', experienc: '08 17 2019' },
            { name: 'Pytorch', img: 'https://www.freepngimg.com/thumb/symbol/72540-network-neural-recurrent-deep-database-pytorch-artificial.png', experienc: '10 08 2019' },
            { name: 'NumPy', img: 'https://user-images.githubusercontent.com/98330/63813335-20cd4b80-c8e2-11e9-9c04-e4dbf7285aa1.png', experienc: '12 18 2019' },
        ]
    },
    {
        head: "Tools & Editors",
        data: [
            { name: 'GitHub', img: 'https://pngimg.com/uploads/github/github_PNG40.png', experienc: '05 10 2015' },
            { name: 'GitLab', img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/GitLab_Logo.svg/260px-GitLab_Logo.svg.png', experienc: '09 18 2017' },
            { name: 'BitBucket', img: 'https://cdn.worldvectorlogo.com/logos/bitbucket-icon.svg', experienc: '10 10 2018' },
            { name: 'Source Tree', img: 'https://cdn.worldvectorlogo.com/logos/sourcetree-1.svg', experienc: '07 16 2017' },
            { name: 'Slack', img: 'https://res.cloudinary.com/startup-grind/image/upload/c_fill,dpr_2.0,f_auto,g_center,h_1080,q_100,w_1080/v1/gcs/platform-data-slack/event_banners/Slack-Profile%40400_mV88AVT.png', experienc: '10 09 2016' },
            { name: 'Postman', img: 'https://www.cediplus.com/img/postman.png', experienc: '06 20 2017' },
            { name: 'Swagger', img: 'https://help.apiary.io/images/swagger-logo.png', experienc: '04 01 2017' },
            { name: 'JIRA', img: 'https://seeklogo.com/images/J/jira-logo-C71F8C0324-seeklogo.com.png', experienc: '02 21 2016' },
            { name: 'Trello', img: 'https://blog.capterra.com/wp-content/uploads/2019/07/trello-mark-blue.png', experienc: '12 02 2016' },
            { name: 'Confluence', img: 'https://images.g2crowd.com/uploads/product/image/large_detail/large_detail_4e59fdb5da6ab1879423c3d7a14a57d1/confluence.png', experienc: '03 29 2016' },
            { name: 'VSCode', img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Visual_Studio_Code_1.35_icon.svg/1200px-Visual_Studio_Code_1.35_icon.svg.png', experienc: '03 01 2017' },
            { name: 'Sublime', img: 'https://cdn.worldvectorlogo.com/logos/sublime-text.svg', experienc: '06 06 2018' },
            { name: 'Android', img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/Android_Studio_icon.svg/1200px-Android_Studio_icon.svg.png', experienc: '02 02 2020' },
        ]
    },
    {
        head: "Operating Systems",
        data: [
            { name: 'Linux', img: 'https://cdn3.iconfinder.com/data/icons/logos-brands-3/24/logo_brand_brands_logos_linux-512.png', experienc: '10 10 2018' },
            { name: 'Mac', img: 'https://www.isupportcause.com/uploads/overlay/isupportimg_1479375745613.png', experienc: '10 25 2019' },
            { name: 'Windows', img: 'https://1000logos.net/wp-content/uploads/2017/06/Windows-Logo.png', experienc: '01 01 2010' },
        ]
    }
]

const Skills = (props: object) => {

    useEffect(() => {
        console.log(
            calcDiff('02 05 2020')
        )
    }, [])

    return (
        <>
            {
                skills.map((skill: any, i: number) => {
                    return (
                        <div className={classes.SkillsWrapper} key={i}>
                            <div className={classes.SkillHeadWrapper}>
                                <div className={classes.SkillsHead} >{skill.head}</div>
                                <span className={classes.SkillsUnderline}></span>
                            </div>
                            <div className={classes.SkillsGrid}>
                                {
                                    skill.data.map((s: any, index: number) => {
                                        return (<Card data={s} key={index} k={index} />)
                                    })
                                }
                            </div>
                            <hr className={classes.LineBreak} />
                        </div>
                    )
                })
            }


        </>
    )
}


export default Skills;