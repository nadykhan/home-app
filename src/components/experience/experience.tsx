import React from 'react';
import classes from './experience.module.scss';
import TimelineItem from './timeline-item/timeline-item'

const timelineItems = [
    {
        name: 'IBM',
        loc: 'Abu Dhabi',
        time: 'Oct 25, 2019 - March 31',
        tech: 'Full Stack Software Engineer',
        desc: 'Started working as a Full-Stack Lead Engineer on TAMM project. TAMM’s portal provides its customers, whether UAE citizens, residents, visitors or investors with a comprehensive range of government services through one single point of access at any time and place. Using TAMMs portal, customers can have direct access to information without having to visit each and every government entity.',
        techs: ['React', 'Redux', 'Node', 'Express', 'TypeScript', 'Elastic Search', 'SonarQube', 'Jest', 'Docker', 'Gitlab', 'Gitlab Pipelines', 'Jira', 'Confluence', 'Microservice Architecture', 'Swagger', 'Postman', 'Slack']
    },
    {
        name: 'Eziline',
        loc: 'Islamabad, Pakistan',
        time: '2018 — 2019',
        tech: 'Full Stack Software Engineer',
        desc: 'Worked as a Full Stack Developer on a Self-Serviced bus ticketing system into your website and let your visitors book bus tickets online. The online bus ticket reservation system is equipped with a powerful set of features allowing you to manage bookings, client data, create reports and translate the booking system without touching a line of code. Remained committed to learning and growing inside this exciting industry. Served as a facilitator for problem-solving and peaceful resolutions.',
        techs: ['EcmaScript', 'TypeScript', 'Angular', 'Ngrx', 'Node', 'Express', 'MySQL', 'Swagger', 'Postgres', 'Docker', 'Kubernetes', 'GitHub', 'JIRA', 'Confluence', 'Slack', 'SonarQube', 'ElasticSearch', 'Azure DevOps', 'Postman', 'Swagger.']
    },
    {
        name: 'Maxtech',
        loc: 'Swat, KP, Pakistan',
        time: '2016 — 2018',
        tech: 'Front End Engineer',
        desc: 'Lead a team to transform the aging application from classic asp to single page modern app. Developed single page application architecture of the new application with React JS & Web APIs. Gave hands-on training to engineers on SPA, React & JavaScript. Developed notifications, Ajax calls, drag & drop components. Worked to build long-term relationships with company stakeholders. Remained committed to learning and growing inside this exciting industry. Worked with a strong attention to detail and a keen eye for projects that need attention.',
        techs: ['JavaScript', 'Jquery', 'PHP', 'Laravel', 'MySQL', 'Google Maps', 'Browser extensions', 'Web Scrapping', 'Slack', 'Swagger.']
    },
    {
        name: 'Teknords',
        loc: 'Swat, KP, Pakistan',
        time: '2012 — 2016',
        tech: 'Full Stack Software Engineer',
        desc: 'Initially joined Teknords, worked for two years as an internee and then started working as a Full Stack Developer. Worked closely with business analysts, managed and trained small team of engineers. Project planning on lower level, tasks assignments, code testing and reviewing were the key roles I was involved in. need attention.',
        techs: ['JavaScript', 'Jquery', 'PHP', 'Laravel', 'MySQL', 'Google Maps', 'Browser extensions', 'Web Scrapping', 'Slack', 'Swagger']
    },
    {
        name: 'Freelance / Remote',
        loc: 'Swat, KP, Pakistan',
        time: '2013 — 2019',
        tech: 'Full Stack Software Engineer',
        desc: 'Outsourced as a remote developer since 2016, worked for some bigger clients in Germany, United States of America, and, United Kingdom. I have developed Web Applications with real time capabilities over Firebase and Socket.IO, Browser Extensions, and Web Scrapping tools using Node JS, Puppeteer. I always had the enthusiasm to ensure the privacy of the information retrieved from the clients and to make the application secure validate the interactive inputs up to the ISMS standards Effectively participated in cross-team strategizing efforts to achieve optimal goals.',
        techs: [],
    },
]

const Experience = (props: any) => {
    return (
        <div className={classes.TimeLineContainer}>

            {
                timelineItems.map((item, index) => {
                    return (
                        <TimelineItem data={item} />
                    )
                })
            }

        </div>
    )
}

export default Experience;