import React from 'react';
import classes from './timeline-item.module.scss';

const TimelineItem = (props: any) => {
    return (
        <div className={classes.TimelineItem}>
            <div className={classes.TechHead}>
                <div className={classes.ItemTech}>{props.data.tech} at </div>
                <div className={classes.ItemCompany}>&nbsp;{props.data.name},&nbsp;</div>
                <div className={classes.ItemLoc}>{props.data.loc}</div>
            </div>
            <div className={classes.ItemTime}>{props.data.time}</div>
            <div className={classes.ItemDesc}>{props.data.desc}</div>
            <div className={classes.ItemTechsGrid}>
                {
                    props.data.techs.map((t: any, i: any) => {
                        return (
                            <span key={i} className={classes.ItemTechnology}>{t}</span>
                        )
                    })
                }
            </div>
            <hr />
        </div>
    )
}

export default TimelineItem