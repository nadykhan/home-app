import moment from 'moment';

export const calcDiff = (from) => {
    const to = new Date()
    let b = moment(from),
        a = moment(to),
        intervals = ['years', 'months', 'days'];

    let out = intervals.map(_in => {
        const diff = a.diff(b, _in);
        b.add(diff, _in);
        return diff !== 0 ? `${diff}${_in}` : null
    }).filter(d => d !== null)


    return out.length !== 0 ? out.join(', ')
        .replace('years', 'y')
        .replace('months', 'm')
        .replace('days', 'd') : "No Experience"

}




