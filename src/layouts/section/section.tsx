import React from 'react';
import classes from './section.module.scss';

const Section = (props: any) => {
    return (
        <div className={classes.Section}>
            <div className={classes.SectionHead}>
                {props.head}
                <span className={classes.SectionLine}></span>
            </div>
            {props.children}
        </div>
    )
}


export default Section