import React from 'react';
import { NavLink } from 'react-router-dom';
import classes from './menu-table.module.scss';

const MenuTable = (props: any) => {
    return (
        <ul className={classes.MenuTable}>
            {
                props.data.map((item: any, index: number) => (
                    <li key={index} >
                        <NavLink
                            exact={item.exact}
                            to={item.path}
                            activeClassName={item.activeClass}> {item.value} </NavLink>
                    </li>
                ))
            }
        </ul>
    )
}


export default MenuTable