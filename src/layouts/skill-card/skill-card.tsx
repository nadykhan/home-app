import React from 'react';
import classes from './skill-card.module.scss'
import { calcDiff } from '../../services/Date/calculate-difference'

const SkillCard = (props: any) => {
    return (
        <div className={classes.SkillCard} key={props.k}>
            <div className={classes.SkillCardImg}>
                <img src={props.data.img} alt='' />
            </div>
            <div className={classes.SkillCardName}>{props.data.name}</div>
            <div className={classes.SkillExperience}><b>Exp</b>. {calcDiff(props.data.experienc)}</div>
        </div>
    )
}

export default SkillCard;