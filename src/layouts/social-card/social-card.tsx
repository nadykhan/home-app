import React from 'react';
import classes from './social-card.module.scss';

const SocialCard = (props: any) => {
    return (
        <a href={props.data.link} target={'_blank'} key={props.data.name} className={classes.SocialCard}>
            <div className={classes.SocialCardImg}>
                <img src={props.data.icon} alt={props.data.name} />
            </div>
            <div className={classes.SocialCardName}>
                {props.data.name}
            </div>
        </a>
    )
}


export default SocialCard;